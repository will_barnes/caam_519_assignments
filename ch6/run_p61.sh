#!/bin/bash
GS=$1
MMS=$2
PC=$3
./problem_6_1 -snes_type newtonls -snes_grid_sequence $GS -da_refine 1 -ksp_rtol 1e-9 -pc_type $PC -mms $MMS -snes_converged_reason -snes_monitor #-ksp_monitor_true_residual