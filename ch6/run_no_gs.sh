for i in `seq 1 6`;
do
    ./problem_6_1 -snes_type newtonls -da_grid_x 17 -da_grid_y 17 -da_refine $i -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 -mg_levels_pc_type sor -pc_mg_type full -mms 1 -snes_monitor -snes_converged_reason -ksp_converged_reason
done
