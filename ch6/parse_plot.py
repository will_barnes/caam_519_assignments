#parse_plot.py
#Will Barnes
#6 November 2015

#Parse output from problem 6 and plot it

#import needed modules
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import subprocess as sp


grid_seq = 4
max_refine = 1

#MMS 1
res_mms_1_lu = np.zeros([grid_seq,3])
res_mms_1_gm = np.zeros([grid_seq,3])

for i in range(1,grid_seq+1):
    #print status
    print("Using MMS 1, LU with grid sequence %d"%i)
    #run petsc for lu
    proc = sp.Popen(['./run_p61.sh',str(i),str(1),'lu'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_1_lu[i-1,0] = int(results_1[1])
    res_mms_1_lu[i-1,1] = float(results_1[4])
    res_mms_1_lu[i-1,2] = float(results_1[6])
    

#MMS 2
res_mms_2_lu = np.zeros([grid_seq,3])
res_mms_2_gm = np.zeros([grid_seq,3])

for i in range(1,grid_seq+1):
    #print status
    print("Using MMS 2, LU with grid sequence %d"%i)
    #run petsc for lu
    proc = sp.Popen(['./run_p61.sh',str(i),str(2),'lu'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_2_lu[i-1,0] = int(results_1[1])
    res_mms_2_lu[i-1,1] = float(results_1[4])
    res_mms_2_lu[i-1,2] = float(results_1[6])
    


#plotting
#part 1
fig = plt.figure()
ax = fig.gca()
ax.plot(res_mms_1_lu[:,0],res_mms_1_lu[:,1],label=r'$\ell_2$')
ax.plot(res_mms_1_lu[:,0],res_mms_1_lu[:,2],label=r'$\ell_{\infty}$')
ax.set_title(r'Convergence Diagram for $u=x(1-x)y(1-y)$',fontsize=18)
ax.set_xlabel(r'Work, $N$',fontsize=18)
ax.set_ylabel(r'error $\epsilon$',fontsize=18)
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend(loc='best',fontsize=14)
plt.savefig('problem_6_1_conv_diag_mms_1.pdf',format='pdf',dpi=1000,bbox_inches='tight')

fig = plt.figure()
ax = fig.gca()
ax.plot(res_mms_2_lu[:,0],res_mms_2_lu[:,1],label=r'$\ell_2$')
ax.plot(res_mms_2_lu[:,0],res_mms_2_lu[:,2],label=r'$\ell_{\infty}$')
ax.set_title(r'Convergence Diagram for $u=\sin{(\pi x)}\sin{(\pi y)}$',fontsize=18)
ax.set_xlabel(r'Work, $N$',fontsize=18)
ax.set_ylabel(r'error $\epsilon$',fontsize=18)
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend(loc='best',fontsize=14)
plt.savefig('problem_6_1_conv_diag_mms_2.pdf',format='pdf',dpi=1000,bbox_inches='tight')
