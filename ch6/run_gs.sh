#!/bin/bash
GS=$1
MMS=$2

./problem_6_1 -snes_type newtonls -snes_grid_sequence $GS -da_refine 1 -pc_type mg -pc_mg_levels 3 -pc_mg_galerkin -mg_levels_ksp_norm_type unpreconditioned -mg_levels_ksp_chebyshev_esteig 0.5,1.2 -mg_levels_pc_type sor -pc_mg_type full -snes_monitor -mms $MMS -snes_converged_reason -ksp_converged_reason
