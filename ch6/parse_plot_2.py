#parse_plot_2.py
#Will Barnes
#7 November 2015

#import needed modules
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import subprocess as sp


grid_seq = 4
max_refine = 1

#MMS 1
res_mms_1_lu = np.zeros([grid_seq,2])
res_mms_1_gm = np.zeros([grid_seq,2])

for i in range(1,grid_seq+1):
    #print status
    print("Using MMS 1, LU with grid sequence %d"%i)
    #run petsc for lu
    proc = sp.Popen(['./run_p61.sh',str(i),str(1),'lu'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_1_lu[i-1,0] = float(results_1[4])
    res_mms_1_lu[i-1,1] = float(results_2[-1])
    #print status
    print("Using MMS 1, GM with grid sequence %d"%i)
    #run petsc for gm
    proc = sp.Popen(['./run_p61.sh',str(i),str(1),'mg'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_1_gm[i-1,0] = float(results_1[4])
    res_mms_1_gm[i-1,1] = float(results_2[-1])
    

#MMS 2
res_mms_2_lu = np.zeros([grid_seq,2])
res_mms_2_gm = np.zeros([grid_seq,2])

for i in range(1,grid_seq):
    #print status
    print("Using MMS 2, LU with grid sequence %d"%i)
    #run petsc for lu
    proc = sp.Popen(['./run_p61.sh',str(i),str(2),'lu'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_2_lu[i-1,0] = float(results_1[4])
    res_mms_2_lu[i-1,1] = float(results_2[-1])
    #print status
    print("Using MMS 2, GM with grid sequence %d"%i)
    #run petsc for gm
    proc = sp.Popen(['./run_p61.sh',str(i),str(2),'mg'],stdout=sp.PIPE)
    #read in from stdout
    results = proc.stdout.readlines()
    results_1 = results[-2].split()
    results_2 = results[-1].split()
    #parse output
    res_mms_2_gm[i-1,0] = float(results_1[4])
    res_mms_2_gm[i-1,1] = float(results_2[-1])
    


#plotting

#part 2
fig = plt.figure()
ax = fig.gca()
ax.plot(res_mms_1_lu[:,1],res_mms_1_lu[:,0],label=r'LU')
ax.plot(res_mms_1_gm[:,1],res_mms_1_gm[:,0],label=r'GMRES')
ax.set_title(r'Work-precision Diagram for $u=x(1-x)y(1-y)$',fontsize=18)
ax.set_xlabel(r'Flops',fontsize=18)
ax.set_ylabel(r'error $\epsilon=\ell_2$',fontsize=18)
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend(loc='best',fontsize=14)
plt.savefig('problem_6_1_wp_diag_mms_1.pdf',format='pdf',dpi=1000,bbox_inches='tight')

fig = plt.figure()
ax = fig.gca()
ax.plot(res_mms_2_lu[:,1],res_mms_2_lu[:,0],label=r'LU')
ax.plot(res_mms_2_gm[:,1],res_mms_2_gm[:,0],label=r'GMRES')
ax.set_title(r'Work-precision Diagram for $u=\sin{(\pi x)}\sin{(\pi y)}$',fontsize=18)
ax.set_xlabel(r'Flops',fontsize=18)
ax.set_ylabel(r'error $\epsilon=\ell_2$',fontsize=18)
ax.set_xscale('log')
ax.set_yscale('log')
ax.legend(loc='best',fontsize=14)
plt.savefig('problem_6_1_wp_diag_mms_2.pdf',format='pdf',dpi=1000,bbox_inches='tight')
