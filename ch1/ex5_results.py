#! /usr/bin/env python2.7
import os
import matplotlib.pyplot as plt
import numpy as np
plt.style.use('ggplot')

sizes = []
times = []
times_ilu = []
times_gamg = []

for k in range (5):
    Nx = 10*2**k 
    modname = 'perf%d' % k
    options = "-da_grid_x %d -da_grid_y %d -log_view :%s.py:ascii_info_detail"%(Nx,Nx,modname)
    os.system('./bin/ex5' + ' ' + options) 
    perfmod = __import__(modname)
    sizes.append(Nx**2)
    times.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    times_ilu.append(perfmod.Stages['Main Stage']['KSPGMRESOrthog'][0]['time'])
    times_gamg.append(perfmod.Stages['Main Stage']['MatILUFactorSym'][0]['time'])
    
print zip(sizes , times)

#plotting
fig = plt.figure(figsize=(10,10))
ax = fig.gca()
ax.plot(sizes,times_ilu,'r',label=r'ILU')
ax.plot(sizes,times_gamg,'k',label=r'KSP/GMRES')
ax.set_yscale('log')
ax.set_xlabel(r'$N$',fontsize=18)
ax.set_ylabel(r'$t$ (s)',fontsize=18)
ax.set_title(r'Comparison between KSP/GMRES and ILU for Ex. 5',fontsize=18)
ax.legend(loc='best')
plt.savefig('hw_1_3_ex5.eps',format='eps')