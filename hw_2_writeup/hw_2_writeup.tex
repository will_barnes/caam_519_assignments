\documentclass[]{amsart}

\usepackage{amsmath,amssymb}
\usepackage{graphicx,subfigure}
\usepackage{tikz}
\usepackage[breaklinks,colorlinks,citecolor=blue]{hyperref}
\usetikzlibrary{}

\title{Homework II: Ch. 3 and Ch. 6}
\author{Will Barnes}
\date{\today}

\begin{document}
	\maketitle
	\section{Problem 3.1}
	%
	Here, we will rework \texttt{ex5} in PETSc using \textit{simple mixing} as opposed to Newton's method. Consider the problem,
	\begin{equation}
		x=Gx,\quad x\in\mathcal{B}.
	\end{equation}
	We will solve this problem using an iterative method of the form,
	\begin{equation}
		x_{i+1} = \mathcal{M}(x_i,\ldots,x_{i-m}).
	\end{equation}
	In \texttt{ex5}, we solve the Bratu equation,
	\begin{equation}
		-\nabla^2u - \lambda e^u = 0,
	\end{equation}
	using Newton's method. Here, we will use a \textit{simple mixing} update rule of the form
	\begin{equation}
		x_{i+1} = x_i + \beta f,
	\end{equation}
	where $f$ is the residual of the Bratu equation and $\beta$ is a constant. Here, we set $\beta=-0.1$. \autoref{fig:wp_diag} a work-precision diagram constructed using runtime as a proxy for the work and problem size as a proxy for precision. This diagram shows that simple mixing as we have implemented it is not as good of a choice of method as Newton's method.
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.7\textwidth]{../ch3/problem_3_1_wp_diag.pdf}
		\caption{Work-precision diagram for Bratu equation using Newton's method and simple mixing.}
		\label{fig:wp_diag}
	\end{figure}
	%
	%
	\clearpage
	\section{Problem 6.1}
	%
	%
	In this problem, we want to solve the modified Bratu equation given by,
	\begin{equation}
		-\nabla\cdot(\tanh{(x-1/2)}\nabla u) - \lambda e^u = 0.
	\end{equation}
	Defining $g(x)\equiv \tanh{(x-1/2)}$, we can rewrite this equation as,
	\begin{eqnarray}
		-\nabla\cdot(g(x)\nabla u) - \lambda e^u = 0, \\
		-\nabla g\cdot\nabla u - g\nabla^2u - \lambda e^u = 0, \\
		-\frac{dg}{dx}\frac{\partial u}{\partial x} - g\nabla^2u - \lambda e^u = 0.
	\end{eqnarray}
	Using grid spacings of $h_x$ and $h_y$ for a rectangular Cartesian grid, the discretization for the Laplacian, using centered differences, can be written as,
	\begin{align}
		\nabla^2u = \frac{u_x(x+h_x/2,y) - u_x(x-h_x/2,y)}{h_x} + \frac{u_y(x,y+h_y/2) - u_y(x,y-h_y/2)}{h_y},\\
		\nabla^2u = \frac{(u(x+h_x,y)-u(x,y))/h_x - (u(x,y)-u(x-h_x,y))/h_x}{h_x} + \\  \frac{(u(x,y+h_y) + u(x,y))/h_y - (u(x,y) - u(x,y-h_y))/h_y}{h_y}, \nonumber\\
		\nabla^2u =  \frac{u(x+h_x,y) - 2u(x,y) + u(x-h_x)}{h_x^2} + \frac{u(x,y+h_y) - 2u(x,y) + u(x,y-h_y)}{h_y^2}.
	\end{align}
	Similarly, we can write the discretization, again using centered differences, for $\partial u/\partial x$ as,
	\begin{align}
		\frac{\partial u}{\partial x} &= \frac{u(x+h_x/2,y) - u(x-h_x/2,y)}{h_x} \\
		\frac{\partial u}{\partial x} &= \frac{(u(x+h_x,y)+u(x,y))/2 - (u(x,y) + u(x-h_x,y))/2}{h_x} \\
		\frac{\partial u}{\partial x} &= \frac{u(x+h_x,y) - u(x-h_x,y)}{2h_x}.
	\end{align}
	\par We evaluate the Jacobian on the five-point stencil in two dimensions as shown in \autoref{fig:stencil}. 
	\begin{figure}[ht]
		\centering
		\include{draw_stencil}
		\caption{Five-point stencil on 2D cartesian grid}
		\label{fig:stencil}
	\end{figure}
	We can define the Jacobian $\mathcal{J}(u)$ as 
	\begin{equation}
		\mathcal{J}(u)\equiv \frac{\partial F}{\partial u},
	\end{equation}
	where $F(u)=\nabla\cdot(\tanh{(x-1/2)\nabla u}) - \lambda e^u$ is the residual. We define for convenience the function $u$ evaluated at the 5 stencil points,
	\begin{align}
		u_C &\equiv u_{j,i} = u(x,y) \\
		u_E &\equiv u_{j,i+1} = u(x+h_x,y), \\
		u_W &\equiv u_{j,i-1} = u(x-h_x,y), \\
		u_S &\equiv u_{j+1,i} = u(x,y+h_y), \\
		u_N &\equiv u_{j-1,i} = u(x,y-h_y).
	\end{align}
	Using these definitions, we can write the residual $F(u)$ as,
	\begin{equation}
		F(u) = -\frac{dg}{dx}\frac{u_E - u_W}{2h_x} - g\left(\frac{u_E - 2u + u_W}{h_x^2} + \frac{u_S - 2u + u_N}{h_y^2}\right) - \lambda e^u.
	\end{equation}
	We evaluate $\mathcal{J}(u)$ at the five stencil points using $\mathcal{J}_k(u)\equiv(\partial F/\partial u)_k$ where $k\in\{N,S,E,W,C\}$,
	\begin{align}
		\mathcal{J}_C &= \frac{2g}{h_x^2} + \frac{2g}{h_y^2} - \lambda e^u, \\
		\mathcal{J}_E &= -\frac{1}{2h_x}\frac{dg}{dx} - \frac{g}{h_x^2}, \\
		\mathcal{J}_W &= \frac{1}{2h_x}\frac{dg}{dx} - \frac{g}{h_x^2}, \\
		\mathcal{J}_S &= -\frac{g}{h_y^2}, \\
		\mathcal{J}_N &= -\frac{g}{h_y^2}.
	\end{align}
	We then use our two proposed MMS solutions,
	\begin{align}
		u_{*,1} = x(1-x)y(1-y), \\
		u_{*,2} = \sin{(\pi x)}\sin{(\pi y)},
	\end{align}
	to test the discretization of our modified Bratu equation.
	\clearpage
	\subsection*{Part I}
	%
	First, we compute convergence graphs for both $u_*$ in $\ell_2$ and $\ell_{\infty}$ norms. We increase the number of degrees of freedom and plot $\ell_2,\ell_{\infty}$ for both $u_*$.
	\begin{figure}[ht]
		\centering
		\subfigure{%
		\includegraphics[width=0.7\textwidth]{../ch6/problem_6_1_conv_diag_mms_1.pdf}
		\label{fig:mms1_conv_diag}}
		\subfigure{%
		\includegraphics[width=0.7\textwidth]{../ch6/problem_6_1_conv_diag_mms_2.pdf}
		\label{fig:mms2_conv_diag}}
		\caption{Convergence diagrams for both MMS solutions using $\ell_2$ and $\ell_{\infty}$.}
	\end{figure}
	%
	\clearpage
	\subsection*{Part II}
	%
	Next, we compute the work-precision diagrams for both of our MMS solutions using LU and GMRES. We use the flops as a proxy for the work and the $\ell_2$ error as a proxy for the precision.
	\begin{figure}[ht]
		\centering
		\subfigure{%
		\includegraphics[width=0.7\textwidth]{../ch6/problem_6_1_wp_diag_mms_1.pdf}
		\label{fig:mms1_wp_diag}}
		\subfigure{%
		\includegraphics[width=0.7\textwidth]{../ch6/problem_6_1_wp_diag_mms_2.pdf}
		\label{fig:mms2_wp_diag}}
		\caption{Work-precision diagrams for both MMS solutions using $\ell_2$.}
	\end{figure}
	
	%
\end{document}