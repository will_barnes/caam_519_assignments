#plot_results_3_1.py
#Will Barnes
#12 October 2015

#Import needed modules
import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

sizes = []
times_simple = []
times_simple_std = []
times_newton = []
times_newton_std = []

options = '-da_grid_x %d -da_grid_y %d -use_anderson %d -log_view :%s.py:ascii_info_detail'
modname = 'perf%d_%d_sc%d'

#clean up old files
try:
    os.system('rm -r __pycache__/')
    os.system('rm perf*.py')
except:
    print("Nothing to remove.")
    pass

n_k = 5
n_sc = 10

for k in range(n_k):
    #Set problem size
    Nx = 10*2**k
    #Append sizes
    sizes.append(Nx)
    #w/out simple mixing
    #subcycle to smooth out fluctuations
    for j in range(n_sc):
        os.system('./problem_3_1 '+options%(Nx,Nx,0,modname%(k,0,j)))
        #w/ Anderson mixing
        os.system('./problem_3_1 '+options%(Nx,Nx,1,modname%(k,1,j)))
    
#loop over produced modules to get times
for k in range(n_k):
    temp_times_n = []
    temp_times_s = []
    for j in range(n_sc):
        perfmod = __import__(modname%(k,0,j))
        temp_times_n.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
        perfmod = __import__(modname%(k,1,j))
        temp_times_s.append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
    times_newton.append(np.mean(temp_times_n))
    times_newton_std.append(np.std(temp_times_n))
    times_simple.append(np.mean(temp_times_s))
    times_simple_std.append(np.std(temp_times_s))
    
#change to numpy arrays for convenience
times_newton = np.array(times_newton)
times_newton_std = np.array(times_newton_std)
times_simple = np.array(times_simple)
times_simple_std = np.array(times_simple_std)

#plotting
fig = plt.figure()
ax = fig.gca()
ax.plot(times_newton,sizes,linewidth=2,color='k',linestyle='--',label=r'Newton')
ax.fill_betweenx(sizes,times_newton-times_newton_std,times_newton+times_newton_std,alpha=0.3,color='gray')
ax.plot(times_simple,sizes,linewidth=2,color='k',label=r'Simple Mixing')
ax.fill_betweenx(sizes,times_simple-times_simple_std,times_simple+times_simple_std,alpha=0.3,color='gray')
ax.set_title(r'Work-precision Diagram, Problem 3.1',fontsize=18)
ax.set_ylabel(r'$N$, problem size',fontsize=18)
ax.set_xlabel(r'$t(s)$, runtime',fontsize=18)
ax.set_yscale('log')
ax.set_xscale('log')
ax.legend(loc='best',fontsize=14)
plt.savefig('problem_3_1_wp_diag.pdf',dpi=1000,format='pdf',bbox_inches='tight')

