#gram_schmidt_performance.py
#Will Barnes
#3 December 2015

import sys
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

def dot_prod(u1,u2):
    if len(u1) != len(u2):
        print("Cannot take dot product of vectors of unequal length.")
        sys.exit()

    dp_sum = 0.
    for i in range(len(u1)):
        dp_sum = dp_sum + u1[i]*u2[i]

    return dp_sum

def norm(u):
    dp = dot_prod(u,u)
    return np.sqrt(dp)


def main():
    #Set number of vs in the set {v}
    N = 100
    #Set number of entries of each v
    M = 1

    #Create v vector to orthogonalize
    np.random.seed()
    v = np.random.rand(M,N)
    #Initialize n orthogonalized vector
    n = np.zeros([M,N])

    #Begin loop over the set of vectors v
    for k in range(N):
        #Read in the kth v vector
        vk = v[:,k]     #M loads per outer loop iteration
        #Initialize the w vector
        wk = vk         #M writes per outer loop iteration
        #Calculate parallel component to be subtracted
        for j in range(k):
            #Read in the jth n vector
            nj = n[:,j]     #M loads per loop iteration
            #Calculate dot product
            dp = dot_prod(vk,nj)    #2M-1 fops per inner loop iteration
            #Update wk vector
            wk -= dp*nj     #2M fops, M writes per inner loop iteration

        #Calculate norm
        wnorm = norm(wk)        #2M fops per outer loop iteration
        #Update n vector
        n[:,k] = wk/wnorm       #M fops, M writes per outer loop iteration

    #Testing
    fig = plt.figure(figsize=(8,8))
    ax = fig.gca()
    dot_array = np.zeros([N,N])
    for i in range(N):
        for j in range(N):
            dot_array[i,j] = dot_prod(n[:,i],n[:,j])

    cax = ax.imshow(dot_array,cmap='hot')
    plt.colorbar(cax)
    plt.show()

if __name__ == "__main__":
    main()
