#calc_beta.py
#Will Barnes
#4 December 2015

import numpy as np
import matplotlib.pyplot as plt

def calc_beta(M):
    b = 8
    numer = 0.5*(M**2 + 2*M - 1) + 1/M
    denom = b*(M+1)
    return numer/denom

m_range = np.arange(1,1e+4,1)
beta_list = []
for m in m_range:
    beta_list.append(calc_beta(m))

fig = plt.figure()
ax = fig.gca()
ax.plot(m_range,beta_list)
ax.set_yscale('log')
ax.set_xscale('log')
plt.show()
