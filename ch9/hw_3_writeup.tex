\documentclass[]{amsart}

\usepackage{amsmath,amssymb,graphicx}
\usepackage{listings}
\usepackage[urlcolor=magenta]{hyperref}
\usepackage{natbib}

\title{Homework III: Ch. 9, CAAM 519}
\author{Will Barnes}
\date{\today}

\begin{document}
	\maketitle
	\section{Problem 9.1}
	%
	Given a set of vectors $\{v_i\}$, we want to create a set of orthonormal vectors $\{n_i\}$ using Gram-Schmidt orthoganlization as implemented using the following algorithm,
	\begin{equation}
		n_k = \frac{w_k}{\|w_k\|},\quad\text{where}\,\,w_k = v_k - \sum_{j<k}(n_j\cdot v_k)n_j.
	\end{equation}
	Let $N$ be the number of $v_i$ in $\{v_i\}$ such that $1\le i\le N$ and let $M$ be the dimension of $v_i$ such that $v_i\in\mathbb{R}^M$. In order to construct an accurate performance model for this algorithm, we will evaluate the balance factor $\beta$, defined as 
	\begin{equation}
		\beta = \frac{\text{Number of flops executed}}{\text{Bytes transferred}},
	\end{equation}
	where $\beta$ is measured in \textit{Keyes} (Ky), or floating-point operations per byte. To evaluate $\beta$, we first have to find the number of floating-point operations and bandwidth for our G-S algorithm. Consider the Python implementation of this orthogonalization procedure below.
	%
	\lstinputlisting[language=Python,firstline=37,lastline=55]{gram_schmidt_perf.py}
	%
	Counting up the number of floating-point operations, including both the inner and outer loops, 
	\begin{align}
		\text{No. of FOPS} =& 2MN + MN + \sum_{i=0}^{N-1}i(2M-1 + M),\\ \nonumber
		=& 3MN + (3M-1)\sum_{i=1}^{N-1}i.
	\end{align}
	Similarly, we the number of loads/writes is
	\begin{align}
		\text{No. of Reads/Writes} =& MN + MN + MN + \sum_{i=0}^{N-1}i(2M),\\ \nonumber
		=& 3MN + 2M\sum_{i=1}^{N-1}i.
	\end{align}
	While the sum $\sum_{i=1}^{n}i$ diverges as $n\to\infty$, we can express the partial sum as
	\begin{equation}
		\sum_{i=1}^{n}i=\frac{1}{2}n(n-1),
	\end{equation}
	for finite $n$, using the well-known triangular number formula \citep{eww_tn}. Plugging all of this into our expression for $\beta$, and using the fact that each read/write is $b$ bytes,
	\begin{equation}
		\beta = \frac{3MN + \frac{N}{2}(3M-1)(N-1)}{(3MN + MN(N-1))b}.
	\end{equation}
	Because we care about the cases where $N\gg1,M\gg1$, we can drop lower order terms. Thus, $\beta$ becomes,
	\begin{equation}
		\beta = \frac{MN(3 + (3-1/M)\frac{1}{2}(N-1))}{MN(3 + (N-1))b}\approx\frac{3+\frac{3}{2}(N-1)}{(3+(N-1))b}\approx\frac{3}{2b}.
	\end{equation}
	Using $b=8$ bytes for \texttt{double}, we find $\beta\approx3/16$ Ky.
	%
	\par From \citep{gb_stats}, I estimated that my mid-2009 Apple Macbook Pro has a peak performance of about $r_{peak}=1600$ F/s. Using the STREAM benchmark in PETSc, I determined that my peak bandwidth is about $b_{peak}=3600$ MB/s based on the Triad test. Thus, I can estimate the bandwidth required to acheive peak performance as,
	\begin{equation}
		b_{req} = \frac{1600}{3/16}\,\frac{\text{MB}}{\text{s}}\,\approx 8500\,\frac{\text{MB}}{\text{s}}.
	\end{equation}
	Similarly, the max performance we can expect from this algorithm given our peak bandwidth can be estimated as,
	\begin{equation}
		r_{max}=\frac{3}{16}3600\,\frac{\text{MF}}{\text{s}}=675\,\frac{\text{MF}}{\text{s}}.
	\end{equation}
	This performance is pretty dismal compared to the peak performance that we've quoted.
	
	\section{Problem 9.2}
	%
	\par From \S9.2 of our class notes, we know that the bandwidth for the Sparse-Matrix Vector Product (SpMV) can be written as,
	\begin{equation}
		\text{bandwidth} = m(b_i + Vb_d) + n_z(b_i + (V+1)b_d), 
	\end{equation}
	where $b_i=4$ bytes for an \texttt{int} and $b_d=8$ bytes for a \texttt{double}. Additionally, $m$ is the number of rows and $n_z$ is the number of non-zero entries. $V$ is the number of vectors that we are multiplying. The number of floating-point operations for this algorithm is just $2n_zV$. Thus, defining $\alpha\equiv n_z/m$, we can calculate $\beta$ as,
	\begin{align}
		\beta =& \frac{2n_zV}{b_i(m+n_z) + b_d(mV + n_z(V+1))},\\
		=& \frac{1}{\frac{b_i}{2V}\left(\frac{m}{n_z}+1\right) + \frac{b_d}{2}\left(\frac{m}{n_z} + \frac{1}{V} + 1\right)},\\
		=& \frac{1}{\left(\frac{2}{V} + 4\right)\alpha^{-1} + \frac{6}{V} + 4}.
	\end{align}
	Notice that here, we have \textbf{not} used the perfect cache assumption. Consider the limit of $V$ such that $V\to\infty$,
	\begin{equation}
		\beta_{\infty}\equiv\lim_{V\to\infty}\beta=\frac{1}{4\alpha^{-1} + 4}.
	\end{equation}
	Thus, we see that this term which we previously ignored in the perfect cache assumption actually dominates at large $V$. Regarding the row occupancy $\alpha$, we saw that in the perfect cache case, as $V\to\infty$, $\beta\to\alpha/8$, meaning $\beta$ would increase linearly with increasing alpha (i.e. as the average row occupancy increases). However, we note that for large $\alpha$ and $V$, in the realistic cache case,
	\begin{equation}
		\lim_{\alpha\to\infty}\frac{1}{4\alpha^{-1} + 4}=\frac{1}{4}.
	\end{equation}
	Thus, $\beta$ flattens out for large $\alpha$, rather than increasing monotonically, when we relax the perfect cache assumption.
	%
	\begin{thebibliography}{30}
		\bibitem{gb_stats}
		"Mac Benchmarks," \textit{Geekbench Browser}, \url{https://browser.primatelabs.com/mac-benchmarks}
		\bibitem{eww_tn}
		Weisstein, Eric W., "Triangular Number," From MathWorld--A Wolfram Web Resource, \url{http://mathworld.wolfram.com/TriangularNumber.html}
	\end{thebibliography}
\end{document}